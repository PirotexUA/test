#ifndef TESTPRJ_H
#define TESTPRJ_H

#include <QtWidgets/QMainWindow>
#include "ui_testprj.h"
#include <QMessageBox>

class TestPrj : public QMainWindow
{
	Q_OBJECT

public:
	TestPrj(QWidget *parent = 0);
	~TestPrj();

private:
	Ui::TestPrjClass ui;

private slots:
	void on_pushButton_clicked();
};

#endif // TESTPRJ_H
