/********************************************************************************
** Form generated from reading UI file 'testprj.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TESTPRJ_H
#define UI_TESTPRJ_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TestPrjClass
{
public:
    QWidget *centralWidget;
    QPushButton *pushButton;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *TestPrjClass)
    {
        if (TestPrjClass->objectName().isEmpty())
            TestPrjClass->setObjectName(QStringLiteral("TestPrjClass"));
        TestPrjClass->resize(600, 400);
        centralWidget = new QWidget(TestPrjClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(270, 160, 75, 23));
        TestPrjClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(TestPrjClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 600, 21));
        TestPrjClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(TestPrjClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        TestPrjClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(TestPrjClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        TestPrjClass->setStatusBar(statusBar);

        retranslateUi(TestPrjClass);

        QMetaObject::connectSlotsByName(TestPrjClass);
    } // setupUi

    void retranslateUi(QMainWindow *TestPrjClass)
    {
        TestPrjClass->setWindowTitle(QApplication::translate("TestPrjClass", "TestPrj", 0));
        pushButton->setText(QApplication::translate("TestPrjClass", "\320\237\321\200\320\276\320\261\320\273\320\265\320\274\320\260", 0));
    } // retranslateUi

};

namespace Ui {
    class TestPrjClass: public Ui_TestPrjClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TESTPRJ_H
